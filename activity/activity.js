db.users.insertMany([

{ "firstName": "Luffy",
   "lastName": "Monkey",
    "email": "MonkeyDLuffy@gmail.com",
    "password": "strawhat",
    "isAdmin": false
},

{
   "firstName": "Zoro",
   "lastName": "Roronoa",
    "email": "RoronoaZoro@gmail.com",
    "password": "swordman",
    "isAdmin": false
}, 
{
    "firstName": "Nami",
   "lastName": "Swan",
    "email": "nami@gmail.com",
    "password": "catburglar",
    "isAdmin": false
},
{
    "firstName": "Usopp",
   "lastName": "God",
    "email": "GodUsopp@gmail.com",
    "password": "sogeking",
    "isAdmin": false
},
{
    "firstName": "Sanji",
   "lastName": "Vinsmoke",
    "email": "PrinceSanji@gmail.com",
    "password": "blackleg",
    "isAdmin": false
}
])
    
    
db.courses.insertMany([
{
      "name":"Pirate 101",
       "price": 1000,
        "isActive": false
},
{
      "name":"Pirate 111",
       "price": 2000,
        "isActive": false
},
{
      "name":"Pirate 150",
       "price":3000,
        "isActive": false
}
])

db.users.find({"isAdmin":false})
db.users.updateOne({"isAdmin": false}, {$set:{"isAdmin":true}})
db.courses.updateOne({"isActive":false}, {$set:{"isActive":true}})
db.courses.deleteMany({"isActive":false})
